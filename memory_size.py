#!/usr/bin/python

# WRITTEN BY NIHAD BAKIRLI (c)
# SINAM LTD COMMUNICATION DEPARTMENT

import os
os.system("ps -aux | awk ' {print $4}' | grep -iv mem > ram.txt" )

lines = []
f = open('ram.txt', "r")
for line in f:
    line = line.strip()
    lines.append(float(line))

f.close()
percent_sum = sum(lines)

ram_capacity = float(os.popen("free -m | grep  Mem | awk '{print $2}' | grep -v used").read())


ram_usage = (ram_capacity * percent_sum ) / 100
a = 5
print ('System usage memory is {}mb now'.format(ram_usage)) 
os.remove("ram.txt")
